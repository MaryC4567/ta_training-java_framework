package com.epam.training.student_mariana_castillo.framework.model;

public enum ProvisioningModel {

    REGULAR("//*[@class='zT2df' and @for='regular']"),
    SPOT("//*[@for='spot']");

    private String xpath;

    private ProvisioningModel (String xpath){
        this.xpath = xpath;
    }

    public String getXpath() {
        return xpath;
    }
}
