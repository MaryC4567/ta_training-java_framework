package com.epam.training.student_mariana_castillo.framework.test;

import com.epam.training.student_mariana_castillo.framework.pages.HomePage;
import com.epam.training.student_mariana_castillo.framework.pages.PricingCalculatorPage;
import com.epam.training.student_mariana_castillo.framework.service.TestData;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PresenceOfElementsTest extends BaseTest{

    @Test
    public void validatePresenceOfSummaryElements(){

        String SEARCH = "testdata.search";

        HomePage home = loadFirstPage();
        PricingCalculatorPage pricing = home.searchPricingCalculator(TestData.getTestData(SEARCH));

        Assert.assertTrue(pricing.presenceOfElements());
    }
}
