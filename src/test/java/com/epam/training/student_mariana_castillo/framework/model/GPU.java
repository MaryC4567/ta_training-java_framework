package com.epam.training.student_mariana_castillo.framework.model;

public class GPU {
    public enum GPUModel{
        NVIDIAV100("//*[@data-value='nvidia-tesla-v100']"),
        NVIDIAP100("//*[@data-value='nvidia-tesla-p100']");

        private String xpath;

        private GPUModel (String xpath){
            this.xpath = xpath;
        }
        public String getXpath() {
            return xpath;
        }
    }

    public enum NumberOfGPUs{
        ONE("//*[@aria-label='Number of GPUs']/li[@data-value='1']"),
        TWO("//*[@aria-label='Number of GPUs']/li[@data-value='2']");
        private String xpath;

        private NumberOfGPUs (String xpath){
            this.xpath = xpath;
        }
        public String getXpath() {
            return xpath;
        }
    }

    private GPU.GPUModel GPUModel;
    private GPU.NumberOfGPUs numberOfGPUs;
    public GPU(GPUModel GPUModel, NumberOfGPUs numberOfGPUs) {
        this.GPUModel = GPUModel;
        this.numberOfGPUs = numberOfGPUs;
    }

    public GPU.GPUModel getGPUModel() {
        return GPUModel;
    }
    public NumberOfGPUs getNumberOfGPUs() {
        return numberOfGPUs;
    }

    public void setGPUModel(GPU.GPUModel GPUModel) {
        this.GPUModel = GPUModel;
    }
    public void setNumberOfGPUs(NumberOfGPUs numberOfGPUs) {
        this.numberOfGPUs = numberOfGPUs;
    }
}
