package com.epam.training.student_mariana_castillo.framework.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@class='mb2a7b']")
    private WebElement searchButton;

    @FindBy(xpath = "//a[@data-ctorig='https://cloud.google.com/products/calculator']")
    private WebElement googleCloudPricing;

    @Override
    public WebDriver getDriver(){
        return super.driver;
    }

    @Override
    public WebDriverWait getWait() {
        return super.wait;
    }

    public PricingCalculatorPage searchPricingCalculator(String search){

        waitToBeClickable(searchButton);
        searchButton.click();
        searchButton.sendKeys(search);
        searchButton.sendKeys(Keys.ENTER);
        waitToBeClickable(googleCloudPricing);
        googleCloudPricing.click();

        return new PricingCalculatorPage(getDriver());
    }
}
