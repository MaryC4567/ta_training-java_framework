package com.epam.training.student_mariana_castillo.framework.model;

public enum LocalSSD {
    GB2X375("//*[@aria-label='Local SSD']/li[3]"),
    GB5X375("//*[@aria-label='Local SSD']/li[6]");

    private String xpath;

    private LocalSSD (String xpath){
        this.xpath = xpath;
    }

    public String getXpath() {
        return xpath;
    }
}
