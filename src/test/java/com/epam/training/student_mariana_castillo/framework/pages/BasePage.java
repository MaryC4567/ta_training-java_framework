package com.epam.training.student_mariana_castillo.framework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public abstract class BasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public abstract WebDriver getDriver();
    public abstract WebDriverWait getWait();

    public void waitElementsToBePresent(String xpath) {
        getWait().until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
    }
    public void waitToBeClickable(WebElement element) {
        getWait().until(ExpectedConditions.elementToBeClickable(element));
    }
    public void waitToBeVisible(WebElement element) {
        getWait().until(ExpectedConditions.visibilityOf(element));
    }

}
