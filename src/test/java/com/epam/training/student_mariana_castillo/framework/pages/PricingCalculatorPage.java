package com.epam.training.student_mariana_castillo.framework.pages;

import com.epam.training.student_mariana_castillo.framework.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PricingCalculatorPage extends BasePage {

    private Logger logger = LogManager.getRootLogger();

    @FindBy(xpath = "//*[@class='UywwFc-vQzf8d']")
    private WebElement addToEstimate;
    @FindBy(xpath = "//*[@class='d5NbRd-EScbFb-JIbuQc PtwYlf']")
    private WebElement computeEngine;
    @FindBy(xpath = "//*[@id='c11']")
    private WebElement instancesNumber;
    @FindBy(xpath = "//span[@id='c22']//following-sibling::div")
    private WebElement operatingSystemElement;
    @FindBy(xpath = "//div[@class='LHK0xb KXFYXb']/div[1]")
    private WebElement machineFamilyElement;
    @FindBy(xpath = "//div[@class='LHK0xb KXFYXb']/div[2]")
    private WebElement seriesElement;
    @FindBy(xpath = "//div[@class='LHK0xb KXFYXb']/div[3]")
    private WebElement machineTypeElement;
    @FindBy(xpath = "//button[@aria-label='Add GPUs']/div[@class='eBlXUe-l6JLsf']/span[@class='eBlXUe-hywKDc']")
    private WebElement addGPUToggle;
    @FindBy(xpath = "//div[@class='qUa9tb'][23]/div/div/div/div/div/div/div")
    private WebElement gpuModelDropdown;
    @FindBy(xpath = "//div[@class='qUa9tb'][24]/div/div/div/div/div/div/div")
    private WebElement numberOfGPUsDropdown;
    @FindBy(xpath = "//div[@class='qUa9tb'][27]/div/div[1]")
    private WebElement localSSDDropdown;
    @FindBy(xpath = "//div[@class='qUa9tb'][29]/div/div/div/div/div/div/div")
    private WebElement regionMenu;
    @FindBy(xpath = "//*[@class='message-container ']/span[2]")
    private WebElement chatBotCloseButton;
    @FindBy(xpath = "//*[@aria-label='Open Share Estimate dialog']/span[6]")
    private WebElement shareButton;
    @FindBy(xpath = "//*[@class='v08BQe']/a")
    private WebElement openEstimateSummary;

    public PricingCalculatorPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public WebDriver getDriver(){
        return super.driver;
    }
    @Override
    public WebDriverWait getWait() {
        return super.wait;
    }

    private void startEstimation() {
        waitToBeClickable(addToEstimate);
        addToEstimate.click();
        waitToBeClickable(computeEngine);
        computeEngine.click();
    }
    private void setInstances(String instances){
        waitToBeClickable(instancesNumber);
        instancesNumber.sendKeys(Keys.DELETE);
        instancesNumber.sendKeys(instances);
    }
    private void selectOperatingSystem(OperatingSystem operatingSystem){
        WebElement operatingSystemOption = driver.findElement(By.xpath(operatingSystem.getXpath()));
        waitToBeClickable(operatingSystemElement);
        operatingSystemElement.click();
        waitToBeClickable(operatingSystemOption);
        operatingSystemOption.click();
    }
    private void setProvisioningModel(ProvisioningModel provisioningModel){
        WebElement provisioningModelOption = driver.findElement(By.xpath(provisioningModel.getXpath()));
        waitToBeClickable(provisioningModelOption);
        provisioningModelOption.click();
    }
    private void selectMachineFamily(MachineType.MachineFamily machineFamily){
        WebElement machineFamilyOption = driver.findElement(By.xpath(machineFamily.getXpath()));
        waitToBeClickable(machineFamilyElement);
        machineFamilyElement.click();
        waitToBeClickable(machineFamilyOption);
        machineFamilyOption.click();
    }
    private void selectSeries(MachineType.Series series){
        WebElement seriesOption = driver.findElement(By.xpath(series.getXpath()));
        waitToBeClickable(seriesElement);
        seriesElement.click();
        waitToBeClickable(seriesOption);
        seriesOption.click();
    }
    private void selectMachineType(MachineType.MachineTypeOption machineType){
        WebElement machineTypeOption = driver.findElement(By.xpath(machineType.getXpath()));
        waitToBeClickable(machineTypeElement);
        machineTypeElement.click();
        waitToBeClickable(machineTypeOption);
        machineTypeOption.click();
    }
    private void addGPUs(GPU gpu){
        waitToBeClickable(addGPUToggle);
        addGPUToggle.click();
        waitToBeClickable(gpuModelDropdown);
        gpuModelDropdown.click();
        WebElement nvidiaTeslaV100 = driver.findElement(By.xpath(gpu.getGPUModel().getXpath()));
        waitToBeClickable(nvidiaTeslaV100);
        nvidiaTeslaV100.click();
        waitToBeClickable(numberOfGPUsDropdown);
        numberOfGPUsDropdown.click();
        WebElement numberOfGPUs = driver.findElement(By.xpath(gpu.getNumberOfGPUs().getXpath()));
        waitToBeClickable(numberOfGPUs);
        numberOfGPUs.click();
    }
    private void setLocalSSDData(LocalSSD localSSD){
        WebElement localSSDOption = driver.findElement(By.xpath(localSSD.getXpath()));
        waitToBeClickable(localSSDDropdown);
        localSSDDropdown.click();
        waitToBeClickable(localSSDOption);
        localSSDOption.click();
    }
    private void selectRegion(Region region){
        WebElement regionOption = driver.findElement(By.xpath(region.getXpath()));
        waitToBeClickable(regionMenu);
        regionMenu.click();
        waitToBeClickable(regionOption);
        regionOption.click();
    }
    private void selectCommittedUseDiscount(CommittedUsage committedUsage){
        WebElement committedUseDiscountOneYear = driver.findElement(By.xpath(committedUsage.getXpath()));
        waitToBeClickable(committedUseDiscountOneYear);
        committedUseDiscountOneYear.click();
    }
    private void closeChatBot(){
        waitToBeClickable(chatBotCloseButton);
        chatBotCloseButton.click();
    }
    private void openEstimateSummary(){
        waitToBeVisible(shareButton);
        shareButton.click();
        waitToBeClickable(openEstimateSummary);
        openEstimateSummary.click();
    }

    public boolean presenceOfElements(){
        startEstimation();
        waitToBeClickable(instancesNumber);
        addGPUToggle.click();
        logger.info("All elements are present in Compute Engine Pricing Calculator");
        return (instancesNumber.isDisplayed()&&operatingSystemElement.isDisplayed()&&
                machineFamilyElement.isDisplayed()&&seriesElement.isDisplayed()&&
                machineTypeElement.isDisplayed()&&addGPUToggle.isDisplayed()&&
                gpuModelDropdown.isDisplayed()&&numberOfGPUsDropdown.isDisplayed()&&
                localSSDDropdown.isDisplayed()&&regionMenu.isDisplayed());
    }

    public CostEstimateSummaryPage computeEnginePricing(ComputeEngine computeEngine) {
        startEstimation();
        setInstances(computeEngine.getNumberOfInstances());
        selectOperatingSystem(computeEngine.getOperatingSystem());
        setProvisioningModel(computeEngine.getProvisioningModel());
        selectMachineFamily(computeEngine.getMachineType().getMachineFamily());
        selectSeries(computeEngine.getMachineType().getSeries());
        selectMachineType(computeEngine.getMachineType().getMachineTypeOption());
        addGPUs(computeEngine.getGpu());
        setLocalSSDData(computeEngine.getLocalSSD());
        selectRegion(computeEngine.getRegion());
        selectCommittedUseDiscount(computeEngine.getCommittedUsage());
        closeChatBot();
        openEstimateSummary();
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }
        logger.info("Summary successfully created");
        return new CostEstimateSummaryPage(getDriver());
    }
}