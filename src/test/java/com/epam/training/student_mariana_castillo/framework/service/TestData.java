package com.epam.training.student_mariana_castillo.framework.service;

import java.util.ResourceBundle;

public abstract class TestData extends ResourceBundle {

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(System.getProperty("environment"));

    public static String getTestData(String key){
        return resourceBundle.getString(key);
    }

}
