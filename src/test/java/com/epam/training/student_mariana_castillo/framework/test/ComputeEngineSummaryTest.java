package com.epam.training.student_mariana_castillo.framework.test;

import com.epam.training.student_mariana_castillo.framework.model.ComputeEngine;
import com.epam.training.student_mariana_castillo.framework.pages.CostEstimateSummaryPage;
import com.epam.training.student_mariana_castillo.framework.pages.HomePage;
import com.epam.training.student_mariana_castillo.framework.pages.PricingCalculatorPage;
import com.epam.training.student_mariana_castillo.framework.service.ComputeEngineCreator;
import com.epam.training.student_mariana_castillo.framework.service.TestData;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ComputeEngineSummaryTest extends BaseTest {

    @Test
    public void computeEngineSummaryValidateData(){

        String SEARCH = "testdata.search";

        ComputeEngine testComputeEngine = ComputeEngineCreator.withCompleteData();
        HomePage home = loadFirstPage();
        PricingCalculatorPage pricing = home.searchPricingCalculator(TestData.getTestData(SEARCH));
        CostEstimateSummaryPage summary = pricing.computeEnginePricing(testComputeEngine);
        summary.switchCase();

        Assert.assertTrue(summary.numberOfInstances(testComputeEngine.getNumberOfInstances()));
        Assert.assertTrue(summary.operatingSystem(TestData.getTestData("testdata.Summary.operatingSystem")));
        Assert.assertTrue(summary.provisioningModel(TestData.getTestData("testdata.Summary.provisioningModel")));
        Assert.assertTrue(summary.addGPUsStatus(TestData.getTestData("testdata.Summary.addGPUsOption")));
        Assert.assertTrue(summary.localSSDCapacity(TestData.getTestData("testdata.Summary.localSSDCapacity")));
        Assert.assertTrue(summary.location(TestData.getTestData("testdata.Summary.locationOption")));
        Assert.assertTrue(summary.committedUseDiscount(TestData.getTestData("testdata.Summary.committedTerm")));
        Assert.assertTrue(summary.machineType(TestData.getTestData("testdata.Summary.machineType")));
        Assert.assertTrue(summary.gpuType(TestData.getTestData("testdata.Summary,gpuTypeOption")));
        Assert.assertTrue(summary.numberOfGPUsSelected(TestData.getTestData("testdata.Summary.gpuNumber")));
    }

}
