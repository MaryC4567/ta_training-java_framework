package com.epam.training.student_mariana_castillo.framework.model;

public enum CommittedUsage {

    ONE_YEAR("//*[@class='qUa9tb'][31]/div/div/div[2]/div/div/div[2]"),
    NONE("//*[@for='none']");

    private final String xpath;

    private CommittedUsage(String xpath){
        this.xpath = xpath;
    }

    public String getXpath() {
        return xpath;
    }


}
