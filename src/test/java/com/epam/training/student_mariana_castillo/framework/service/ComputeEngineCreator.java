package com.epam.training.student_mariana_castillo.framework.service;

import com.epam.training.student_mariana_castillo.framework.model.*;

public class ComputeEngineCreator {

    public static final String INSTANCES = "testdata.ComputeEngine.numberOfInstances";
    public static final String OPERATING_SYSTEM = "testdata.ComputeEngine.operatingSystem";
    public static final String PROVISIONING_MODEL = "testdata.ComputeEngine.provisioningModel";
    public static final MachineType MACHINE_TYPE = new MachineType(MachineType.MachineFamily
            .valueOf(TestData.getTestData("testdata.ComputeEngine.machineFamily")),
            MachineType.Series.valueOf(TestData.getTestData("testdata.ComputeEngine.series")),
            MachineType.MachineTypeOption.valueOf(TestData.getTestData("testdata.ComputeEngine.machineType")));
    public static final GPU GPU_INSTANCE = new GPU(GPU.GPUModel.
            valueOf(TestData.getTestData("testdata.ComputeEngine.gpuModel")),
            GPU.NumberOfGPUs.valueOf(TestData.getTestData("testdata.ComputeEngine.numberOfGPUs")));
    public static final String LOCAL_SSD = "testdata.ComputeEngine.localSSD";
    public static final String REGION = "testdata.ComputeEngine.region";
    public static final String COMMITTED_USAGE = "testdata.ComputeEngine.committedUsage";

    public static ComputeEngine withCompleteData(){
        return new ComputeEngine(TestData.getTestData(INSTANCES),
                OperatingSystem.valueOf(TestData.getTestData(OPERATING_SYSTEM)),
                ProvisioningModel.valueOf(TestData.getTestData(PROVISIONING_MODEL)),
                MACHINE_TYPE, GPU_INSTANCE,
                LocalSSD.valueOf(TestData.getTestData(LOCAL_SSD)),
                Region.valueOf(TestData.getTestData(REGION)), CommittedUsage.valueOf(TestData.getTestData(COMMITTED_USAGE)));
    }
}
