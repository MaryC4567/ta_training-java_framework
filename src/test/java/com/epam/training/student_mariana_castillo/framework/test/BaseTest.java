package com.epam.training.student_mariana_castillo.framework.test;

import com.epam.training.student_mariana_castillo.framework.driver.Driver;
import com.epam.training.student_mariana_castillo.framework.pages.HomePage;
import com.epam.training.student_mariana_castillo.framework.service.TestData;
import com.epam.training.student_mariana_castillo.framework.utils.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public class BaseTest {

    public static WebDriver myDriver;

    @BeforeMethod
    public static void openUrl() {
        String url = "testdata.ComputeEngine.URL";
        myDriver = Driver.getDriver();
        navigateTo(TestData.getTestData(url));
    }

    public static void navigateTo(String url) {
        Driver.getDriver().navigate().to(url);
    }

    public static HomePage loadFirstPage() {
        return new HomePage(Driver.getDriver());
    }

    @AfterSuite
    public static void closeDriver() {
        Driver.closeDriver();
    }
}
