package com.epam.training.student_mariana_castillo.framework.model;

public class ComputeEngine {

    private String numberOfInstances;
    private OperatingSystem operatingSystem;
    private ProvisioningModel provisioningModel;
    private MachineType machineType;
    private GPU gpu;
    private LocalSSD localSSD;
    private Region region;
    private CommittedUsage committedUsage;

    public ComputeEngine(String numberOfInstances, OperatingSystem operatingSystem,
                         ProvisioningModel provisioningModel, MachineType machineType, GPU gpu,
                         LocalSSD localSSD, Region region, CommittedUsage committedUsage) {
        this.numberOfInstances = numberOfInstances;
        this.operatingSystem = operatingSystem;
        this.provisioningModel = provisioningModel;
        this.machineType = machineType;
        this.gpu = gpu;
        this.localSSD = localSSD;
        this.region = region;
        this.committedUsage = committedUsage;
    }
    public String getNumberOfInstances() {
        return numberOfInstances;
    }
    public OperatingSystem getOperatingSystem() {
        return operatingSystem;
    }
    public ProvisioningModel getProvisioningModel() {
        return provisioningModel;
    }
    public MachineType getMachineType() {
        return machineType;
    }
    public GPU getGpu() {
        return gpu;
    }
    public LocalSSD getLocalSSD() {
        return localSSD;
    }
    public Region getRegion() {
        return region;
    }
    public CommittedUsage getCommittedUsage() {
        return committedUsage;
    }

    public void setNumberOfInstances(String numberOfInstances) {
        this.numberOfInstances = numberOfInstances;
    }
    public void setOperatingSystem(OperatingSystem operatingSystem) {
        this.operatingSystem = operatingSystem;
    }
    public void setProvisioningModel(ProvisioningModel provisioningModel) {
        this.provisioningModel = provisioningModel;
    }
    public void setMachineType(MachineType machineType) {
        this.machineType = machineType;
    }
    public void setGpu(GPU gpu) {
        this.gpu = gpu;
    }
    public void setLocalSSD(LocalSSD localSSD) {
        this.localSSD = localSSD;
    }
    public void setRegion(Region region) {
        this.region = region;
    }
    public void setCommittedUsage(CommittedUsage committedUsage) {
        this.committedUsage = committedUsage;
    }
}
