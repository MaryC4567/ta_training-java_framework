package com.epam.training.student_mariana_castillo.framework.model;

public class MachineType {

    public enum MachineFamily{
        GENERAL_PURPOSE("//*[@data-value='general-purpose']");
        private String xpath;

        private MachineFamily (String xpath){
            this.xpath = xpath;
        }
        public String getXpath() {
            return xpath;
        }
    }
    public enum Series{
        N1("//*[@data-value='n1']"),
        N2("//*[@data-value='n2']");
        private String xpath;

        private Series (String xpath){
            this.xpath = xpath;
        }

        public String getXpath() {
            return xpath;
        }
    }
    public enum MachineTypeOption{
        N1_STANDARD_8("//*[@data-value='n1-standard-8']"),
        N1_STANDARD_32("//*[@data-value='n1-standard-32']");
        private String xpath;

        private MachineTypeOption (String xpath){
            this.xpath = xpath;
        }

        public String getXpath() {
            return xpath;
        }
    }

    private MachineFamily machineFamily;
    private Series series;
    private MachineTypeOption machineTypeOption;

    public MachineType(MachineFamily machineFamily, Series series, MachineTypeOption machineTypeOption){
        this.machineFamily = machineFamily;
        this.series = series;
        this.machineTypeOption = machineTypeOption;
    }

    public MachineFamily getMachineFamily() {
        return machineFamily;
    }
    public Series getSeries() {
        return series;
    }
    public MachineTypeOption getMachineTypeOption() {
        return machineTypeOption;
    }
    public void setMachineFamily(MachineFamily machineFamily) {
        this.machineFamily = machineFamily;
    }
    public void setSeries(Series series) {
        this.series = series;
    }
    public void setMachineTypeOption(MachineTypeOption machineTypeOption) {
        this.machineTypeOption = machineTypeOption;
    }
}
