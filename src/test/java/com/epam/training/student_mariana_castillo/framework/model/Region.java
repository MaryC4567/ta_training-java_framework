package com.epam.training.student_mariana_castillo.framework.model;

public enum Region {

    NETHERLANDS("//*[@data-value='europe-west4']"),
    BELGIUM("//*[@data-value='europe-west1']");

    private String xpath;

    private Region (String xpath){
        this.xpath = xpath;
    }

    public String getXpath() {
        return xpath;
    }

}
