package com.epam.training.student_mariana_castillo.framework.model;

public enum OperatingSystem {

    DEBIAN("//*[@data-value='free-debian-centos-coreos-ubuntu-or-byol-bring-your-own-license']"),
    UBUNTU("//*[@data-value='paid-ubuntu-pro']");

    private String xpath;

    private OperatingSystem (String xpath){
        this.xpath = xpath;
    }

    public String getXpath() {
        return xpath;
    }
}
