## ta_training-java_framework

# Overview
This repository contains a test automation framework for Google Cloud Platform Pricing Calculator https://cloud.google.com/products/calculator?hl=en

# Details
Selenium 4.21.0
Java  17.0.11
TestNG 7.10.2

# Developed by
Mariana Castillo López